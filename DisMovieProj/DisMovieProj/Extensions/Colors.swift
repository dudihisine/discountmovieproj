//
//  Colors.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 03/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

public enum Colors {

    public static let labelDarkMode: UIColor = UIColor(red: 247/255.0, green: 250/255.0, blue: 251/255.0, alpha: 1.0)

    public static let labelLightMode: UIColor = UIColor(red: 24/255.0, green: 26/255.0, blue: 37/255.0, alpha: 1.0)

    public static let labelGrayDarkMode: UIColor = UIColor.darkGray

    public static let labelGrayLightMode: UIColor = UIColor.lightGray

    public static let mainBGDarkMode: UIColor = UIColor(red: 20/255.0, green: 20/255.0, blue: 20/255.0, alpha: 1.0)

    public static let mainBGLightMode: UIColor = UIColor(red: 242/255.0, green: 242/255.0, blue: 247/255.0, alpha: 1.0)

    public static let linkDarkMode: UIColor = UIColor(red: 129/255.0, green: 229/255.0, blue: 217/255.0, alpha: 1.0)

    public static let linkLightMode: UIColor = UIColor(red: 56/255.0, green: 178/255.0, blue: 172/255.0, alpha: 1.0)

    public static var label: UIColor = {
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    /// Return the color for Dark Mode
                    return Colors.labelDarkMode
                } else {
                    /// Return the color for Light Mode
                    return Colors.labelLightMode
                }
            }
        } else {
            /// Return a fallback color for iOS 12 and lower.
            return Colors.labelLightMode
        }
    }()

    public static var labelHighlited: UIColor = UIColor(red: 248/255.0, green: 181/255.0, blue: 18/255.0, alpha: 1.0)

    public static var labelGray: UIColor = {
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    /// Return the color for Dark Mode
                    return Colors.labelGrayDarkMode
                } else {
                    /// Return the color for Light Mode
                    return Colors.labelGrayLightMode
                }
            }
        } else {
            /// Return a fallback color for iOS 12 and lower.
            return Colors.labelGrayLightMode
        }
    }()

    public static var mainBG: UIColor = {
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    /// Return the color for Dark Mode
                    return Colors.mainBGDarkMode
                } else {
                    /// Return the color for Light Mode
                    return Colors.mainBGLightMode
                }
            }
        } else {
            /// Return a fallback color for iOS 12 and lower.
            return Colors.mainBGLightMode
        }
    }()

    public static var navigationBar: UIColor = {
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                    if UITraitCollection.userInterfaceStyle == .dark {
                        /// Return the color for Dark Mode
                        return Colors.movieCellBGDarkMode
                    } else {
                        /// Return the color for Light Mode
                        return Colors.movieCellBGDLightMode
                    }
                }
            } else {
                /// Return a fallback color for iOS 12 and lower.
                return Colors.movieCellBGDLightMode
            }
    }()

    public static var link: UIColor = {
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    /// Return the color for Dark Mode
                    return Colors.linkDarkMode
                } else {
                    /// Return the color for Light Mode
                    return Colors.linkLightMode
                }
            }
        } else {
            /// Return a fallback color for iOS 12 and lower.
            return Colors.linkLightMode
        }
    }()

    public static var shadow: UIColor = UIColor.black

    //MARK:MoviewCell
    public static let movieCellBGDarkMode: UIColor = UIColor(red: 28/255.0, green: 28/255.0, blue: 30/255.0, alpha: 1.0)

    public static let movieCellBGDLightMode: UIColor = UIColor.white

    public static var movieCellBG: UIColor = {
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    /// Return the color for Dark Mode
                    return Colors.movieCellBGDarkMode
                } else {
                    /// Return the color for Light Mode
                    return Colors.movieCellBGDLightMode
                }
            }
        } else {
            /// Return a fallback color for iOS 12 and lower.
            return Colors.movieCellBGDLightMode
        }
    }()
}
