//
//  DataExtension.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 02/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import Foundation

extension Data{
    func toString()->String{
        return String(decoding: self, as: UTF8.self)
    }
}
