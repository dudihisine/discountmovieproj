//
//  StoryboardExtension.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 04/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

public protocol Storyboarded {
    static func instantiate(fromStoryboard storyboard: String) -> Self
}

extension Storyboarded where Self: UIViewController {
    public static func instantiate(fromStoryboard storyboard: String) -> Self {
        let viewControllerIdentifier = String(describing: self)
        let storyboard = UIStoryboard(name: storyboard, bundle: Bundle.main)

        if let controller = storyboard.instantiateViewController(withIdentifier: viewControllerIdentifier) as? Self {
            return controller
        } else {
            fatalError("Could not instantiate viewcontroller with identifier \"\(viewControllerIdentifier)\"")
        }
    }
}

extension UIViewController: Storyboarded {}
