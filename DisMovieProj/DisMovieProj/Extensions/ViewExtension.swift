//
//  ViewExtension.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 03/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

extension UIView{
    func fixInView(subView : UIView) -> Void{
        subView.translatesAutoresizingMaskIntoConstraints = false;
        subView.frame = self.frame;
        self.addSubview(subView);
        NSLayoutConstraint(item: subView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: subView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: subView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: subView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}
