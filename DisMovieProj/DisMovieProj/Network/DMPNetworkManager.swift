//
//  DMPNetworkManager.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 01/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit
import Alamofire

enum DMPError: Error {
    case parse
    case request
    case general(reason: String)

    func getStringValue()->String{
        switch self {
        case .parse:
            return "parse error"
        case .request:
            return "request error"
        case .general(let reason):
            return reason
        }
    }
}

enum DMPResult<T> {
    case success(T)
    case failure(DMPError)
}

class DMPNetworkManager {
    
    static func request<U, T: DMPRequest>(request: T, decodeType: U.Type, completion: @escaping (DMPResult<U?>) -> Void) where U: Decodable {
        guard let urlRequest = buildUrlRequest(for: request) else {
            debugPrint("Error")
            completion(.failure(.request))
            return
        }

        //MARK:Send request
        DMPNetworkSession.defaultManager.request(urlRequest).responseJSON { (response) in
            debugPrint("Result:\n\(response.result)\nData:\n\(response.data?.toString() ?? "No data")" )

            switch response.result{
            case .success(let response):
                //MARK:Request succeed
                if let dataDecode = self.decodingData(decodingType:decodeType, response: response){
                    debugPrint("Response parse")
                    completion(.success(dataDecode))
                }else {
                    debugPrint("Response could not be parse")
                    completion(.failure(.parse))
                }
            case .failure(let error):
                //MARK:Request failed
                debugPrint("Response failed with error: \(error.localizedDescription)")
                //not show error popup if cancel (back press or exit)
                if (error as NSError?)?.code != -999 {
                    completion(.failure(.general(reason: error.localizedDescription)))
                }
            }
        }
    }

    static fileprivate func buildUrlRequest(for request : DMPRequest) -> URLRequestConvertible? {
        //MARK:Build URL

        var urlStr = "\(DMPNetworkSession.baseURL)\(request.endPoint)"

        //MARK:Add Path Parameters
        if let pathParam = request.pathParams{
            urlStr += "/\(pathParam)"
        }

        //MARK:Build url request
        guard var finalURL = URL(string: urlStr) else {
            debugPrint("URL could not be built")
            return nil
        }

        //MARK:Add Default Query Params

        if var queryItems = DMPNetworkSession.defaultQueryParams{
            guard let urlComponents = NSURLComponents(string: urlStr) else {
                debugPrint("URL could not be built")
                return nil
            }

            //MARK:Add Custom Query Params
            if let customQueryParams = request.queryParams{
                queryItems.merge(dict: customQueryParams)
            }

            urlComponents.queryItems = queryItems.map {
                URLQueryItem(name: $0.key, value: $0.value)
            }

            if let url = urlComponents.url{
                finalURL = url
            }
        }
        
        do {

            //MARK:Return final url
            let urlRequest = try URLRequest(url: finalURL, method: HTTPMethod(rawValue: request.method.rawValue))
            debugPrint("URL:\(urlRequest))")
            return urlRequest
        } catch _ {
            debugPrint("Cant build request")
            return nil
        }
    }

    static fileprivate func decodingData<U>(decodingType: U.Type, response: Any) -> (U?) where U: Decodable{
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: response)
            return try JSONDecoder().decode(decodingType, from: jsonData)
        } catch let parsingError {
            debugPrint("Error parse: \(parsingError)")
            return nil
        }
    }
}

