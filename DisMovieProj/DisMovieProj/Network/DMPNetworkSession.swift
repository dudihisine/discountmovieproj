//
//  DMPNetworkSession.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 02/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import Foundation
import Alamofire

protocol DMPNetworkConfiguration {
    static var apiKey: String { get }
    static var baseURL: String { get }
    static var timeoutInterval: Double { get }
    static var defaultQueryParams: [String:String]? { get }
}

struct URI{
    static let base = "https://api.themoviedb.org/3/"
    static let basePoster = "https://image.tmdb.org/t/p/w780/"
}

class DMPNetworkSession: DMPNetworkConfiguration{

    static var defaultManager: Alamofire.Session = {

        let configuration = URLSessionConfiguration.default

        configuration.timeoutIntervalForRequest = timeoutInterval

        let manager = Alamofire.Session(
            configuration: configuration
        )
        return manager
    }()

    static var apiKey : String {
        return "352282b0b465fb10247d7aa2d871cacb"
    }

    static var baseURL: String {
        return URI.base
    }

    static var timeoutInterval: Double {
        return 1 * 60.0
    }

    static var defaultQueryParams: [String : String]?{
        return ["api_key":apiKey,"language":"en-US"]
    }
}
