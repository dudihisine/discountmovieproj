//
//  Genre.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 02/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

struct Genre: Decodable{
    var id : Int?
    var name : String?
}
