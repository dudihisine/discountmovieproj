//
//  Movie.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 02/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

class Movie: Decodable{

    var posterPath : String?
    var adult : Bool?
    var overview : String?
    var releaseDate : String?
    var genreIds : [Int]?
    var id : Int?
    var originalTitle : String?
    var originalLang : String?
    var title : String?
    var backdropPath : String?
    var popularity : Float?
    var voteCount : Int?
    var video : Bool?
    var voteAverage : Float?

    private enum CodingKeys: String, CodingKey {
        case poster_path
        case adult
        case overview
        case release_date
        case genre_ids
        case id
        case original_title
        case original_language
        case title
        case backdrop_path
        case popularity
        case vote_count
        case video
        case vote_average
    }

    required init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        posterPath = try values.decodeIfPresent(String.self, forKey: .poster_path)
        adult = try values.decodeIfPresent(Bool.self, forKey: .adult)
        overview = try values.decodeIfPresent(String.self, forKey: .overview)
        releaseDate = try values.decodeIfPresent(String.self, forKey: .release_date)
        genreIds = try values.decodeIfPresent([Int].self, forKey: .genre_ids)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        originalTitle = try values.decodeIfPresent(String.self, forKey: .original_title)
        originalLang = try values.decodeIfPresent(String.self, forKey: .original_language)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        backdropPath = try values.decodeIfPresent(String.self, forKey: .backdrop_path)
        popularity = try values.decodeIfPresent(Float.self, forKey: .popularity)
        voteCount = try values.decodeIfPresent(Int.self, forKey: .vote_count)
        video = try values.decodeIfPresent(Bool.self, forKey: .video)
        voteAverage = try values.decodeIfPresent(Float.self, forKey: .vote_average)
    }
}

