//
//  Video.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 02/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

class Video: Decodable{

    var id : String?
    var iso6391 : String?
    var iso31661 : String?
    var key : String?
    var name : String?
    var site : String?
    var size : Float?
    var type : String?

    private enum CodingKeys: String, CodingKey {
        case id
        case iso_639_1
        case iso_3166_1
        case key
        case name
        case site
        case size
        case type
    }

    required init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        iso6391 = try values.decodeIfPresent(String.self, forKey: .iso_639_1)
        iso31661 = try values.decodeIfPresent(String.self, forKey: .iso_3166_1)
        key = try values.decodeIfPresent(String.self, forKey: .key)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        site = try values.decodeIfPresent(String.self, forKey: .site)
        size = try values.decodeIfPresent(Float.self, forKey: .size)
        type = try values.decodeIfPresent(String.self, forKey: .type)
    }
}
