//
//  DMPGetGenreRequest.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 01/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit
import Alamofire

class DMPGetGenreRequest: DMPRequest {

    var endPoint: String{
        return "genre/movie/list"
    }

    var method: HTTPMethod{
        return .get
    }

    var pathParams: String?{
        return nil
    }

    var queryParams: [String:String]?{
        return nil
    }
}
