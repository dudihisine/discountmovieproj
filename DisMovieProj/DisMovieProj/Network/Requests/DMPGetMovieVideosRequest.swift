//
//  DMPGetMovieVideosRequest.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 01/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit
import Alamofire

class DMPGetMovieVideosRequest: DMPRequest {

    var movieID : String = ""

    init(movieID : String) {
        self.movieID = movieID
    }

    var endPoint: String{
        return "movie"
    }

    var method: HTTPMethod{
        return .get
    }

    var pathParams: String?{
        return "\(movieID)/videos"
    }

    var queryParams: [String:String]?{
        return nil
    }
}
