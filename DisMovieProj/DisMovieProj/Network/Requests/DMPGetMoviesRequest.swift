//
//  DMPGetMoviesRequest.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 01/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit
import Alamofire

class DMPGetMoviesRequest: DMPRequest {

    var page : Int = 1

    init(page : Int) {
        self.page = page
    }

    var endPoint: String{
        return "movie/popular"
    }

    var method: HTTPMethod{
        return .get
    }

    var pathParams: String?{
        return nil
    }

    var queryParams: [String:String]?{
        return ["page" : String(page)]
    }
}
