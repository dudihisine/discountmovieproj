//
//  DMPRequest.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 01/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit
import Alamofire

protocol DMPRequest {
    var endPoint: String { get }
    var method: HTTPMethod { get }
    var pathParams: String? { get }
    var queryParams: [String:String]? { get }
}
