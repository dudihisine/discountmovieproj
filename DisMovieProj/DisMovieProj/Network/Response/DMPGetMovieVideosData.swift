//
//  DMPGetMovieVideosData.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 01/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

struct DMPGetMovieVideosData: Decodable {
    var id : Int?
    var results : [Video]?
}
