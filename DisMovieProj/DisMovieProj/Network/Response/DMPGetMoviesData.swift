//
//  DMPGetMoviesData.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 01/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

class DMPGetMoviesData: Decodable {

    var page : Int?
    var results : [Movie]?
    var totalResults : Int?
    var totalPages : Int?

    private enum CodingKeys: String, CodingKey {
        case page
        case results
        case total_results
        case total_pages
    }

    required init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        page = try values.decode(Int.self, forKey: .page)
        results = try values.decode([Movie].self, forKey: .results)
        totalResults = try values.decode(Int.self, forKey: .total_results)
        totalPages = try values.decode(Int.self, forKey: .total_pages)
    }

}
