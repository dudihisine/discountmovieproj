//
//  DMPGenreCell.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 04/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

class DMPGenreCell: UICollectionViewCell {

    static let cellIdentifier = "DMPGenreCell"

    //MARK:Outlets
    @IBOutlet weak var vRoundBG: DMPRoundView!
    @IBOutlet weak var lblTitle: UILabel!

    //MARK:Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        setpUI()
    }

    //MARK:Setup
    func setpUI(){
        contentView.backgroundColor = Colors.mainBG
        lblTitle.textColor = Colors.labelHighlited
        vRoundBG.backgroundColor = Colors.movieCellBG
        vRoundBG.shadowColor = Colors.shadow
    }

    func setupCell(title : String?, shouldHighlight : Bool){
        lblTitle.text = title
        vRoundBG.backgroundColor = shouldHighlight ? Colors.labelHighlited : Colors.movieCellBG
        lblTitle.textColor = shouldHighlight ? Colors.label : Colors.labelHighlited

        //Accesibility
        lblTitle.accessibilityTraits = shouldHighlight ? [UIAccessibilityTraits.button,UIAccessibilityTraits.selected] : [UIAccessibilityTraits.button]
    }

}
