//
//  DMPVideoCell.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 04/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit
import AlamofireImage

class DMPVideoCell: UICollectionViewCell {

    static let cellIdentifier = "DMPVideoCell"
    static let cellRatio : CGFloat = 16/9.0

    //MARK:Outlets
    @IBOutlet weak var imgVideoThumbnail: DMPRoundImageView!

    //MARK:Setup
    func setupCell(for video : DMPVideoViewData, index : Int){
        if let videoThumbnailURL = video.thumbnailImageURL{
            imgVideoThumbnail.af.setImage(withURL: videoThumbnailURL,  cacheKey:videoThumbnailURL.absoluteString, placeholderImage: UIImage(named: "posterPlaceholder"))
        }

        setupAccessibility(for: index)
    }

    func setupAccessibility(for index : Int){
        imgVideoThumbnail.accessibilityTraits = [UIAccessibilityTraits.button]
        imgVideoThumbnail.accessibilityLabel = "Video Number \(index)"
    }

}
