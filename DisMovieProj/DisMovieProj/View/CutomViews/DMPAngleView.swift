//
//  DMPAngleView.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 03/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

@IBDesignable
class DMPAngleView: UIView {

    @IBInspectable public var fillColor: UIColor = .blue { didSet { setNeedsLayout() } }

    @IBInspectable var shadowColor: UIColor? {
        get {
            if let color = shapeLayer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                shapeLayer.shadowColor = color.cgColor
            } else {
                shapeLayer.shadowColor = nil
            }
        }
    }

    @IBInspectable var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            shapeLayer.shadowOpacity = newValue
        }
    }

    @IBInspectable var shadowOffset: CGPoint {
        get {
            return CGPoint(x: shapeLayer.shadowOffset.width, y:shapeLayer.shadowOffset.height)
        }
        set {
            shapeLayer.shadowOffset = CGSize(width: newValue.x, height: newValue.y)
        }

    }

    @IBInspectable var shadowBlur: CGFloat {
        get {
            return shapeLayer.shadowRadius
        }
        set {
            shapeLayer.shadowRadius = newValue / 2.0
        }
    }

    @IBInspectable var shadowSpread: CGFloat = 0 {
        didSet {
            if shadowSpread == 0 {
                layer.shadowPath = nil
            } else {
                let dx = -shadowSpread
                let rect = bounds.insetBy(dx: dx, dy: dx)
                layer.shadowPath = UIBezierPath(rect: rect).cgPath
            }
        }
    }

    var points: [CGPoint] = [
        CGPoint(x: 0, y: 1),
        CGPoint(x: 1, y: 1),
        CGPoint(x: 1, y: 0.6),
        CGPoint(x: 0, y: 1)
    ] { didSet { setNeedsLayout() } }

    private lazy var shapeLayer: CAShapeLayer = {
        let _shapeLayer = CAShapeLayer()
        self.layer.insertSublayer(_shapeLayer, at: 0)
        return _shapeLayer
    }()

    override public func layoutSubviews() {
        shapeLayer.fillColor = fillColor.cgColor

        guard points.count > 2 else {
            shapeLayer.path = nil
            return
        }

        let path = UIBezierPath()

        path.move(to: convert(relativePoint: points[0]))
        for point in points.dropFirst() {
            path.addLine(to: convert(relativePoint: point))
        }
        path.close()

        shapeLayer.path = path.cgPath
    }

    private func convert(relativePoint point: CGPoint) -> CGPoint {
        return CGPoint(x: point.x * bounds.width + bounds.origin.x, y: point.y * bounds.height + bounds.origin.y)
    }

}
