//
//  DMPBaseCustomView.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 02/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

class DMPBaseCustomView: UIView {

    @IBOutlet var contentView: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    func commonInit() {
        Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)
        fixInView(subView: contentView)
        sendSubviewToBack(contentView)
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        sendSubviewToBack(contentView)
    }

}
