//
//  DMPRoundImageView.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 02/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

@IBDesignable
class DMPRoundImageView: UIImageView {

    @IBInspectable var cornerRadius: Double {
          get {
            return Double(self.layer.cornerRadius)
          }
          set {
           self.layer.cornerRadius = CGFloat(newValue)
          }
    }

}
