//
//  DMPLoadingView.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 02/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

class DMPLoadingView: DMPBaseCustomView {

    //MARK:Outlets
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnTryAgain: UIButton!

    //MARK:Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        btnTryAgain.isHidden = true
        btnTryAgain.isUserInteractionEnabled = true
        indicator.color = Colors.link
        lblMessage.textColor = Colors.label
        btnTryAgain.tintColor = Colors.link

        setupAccessibility()
    }

    func setupAccessibility(){
        indicator.accessibilityElementsHidden = true
    }
}
