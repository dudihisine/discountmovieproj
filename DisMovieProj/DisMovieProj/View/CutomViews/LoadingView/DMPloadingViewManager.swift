//
//  DMPloadingViewManager.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 02/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

protocol DMPLoadingViewDelegate: AnyObject {
    func didTapPressedAgain()
}

class DMPloadingViewManager: NSObject {

    //MARK:Veriables
    private var loadingView = DMPLoadingView()
    private weak var delegate: DMPLoadingViewDelegate?

    //MARK:Instance
    init(delegate: DMPLoadingViewDelegate?) {
        self.delegate = delegate
    }

    //MARK:Methods
    func showLoading(superView: UIView) {
        setupView(inSuperview: superView)

        loadingView.lblMessage.text = "Loading..."
        loadingView.btnTryAgain.isHidden = true
        loadingView.btnTryAgain.isUserInteractionEnabled = false
        loadingView.indicator.startAnimating()
    }

    func showError(superView: UIView, error: DMPError) {
        setupView(inSuperview: superView)

        loadingView.lblMessage.text = error.getStringValue()
        loadingView.indicator.stopAnimating()
        loadingView.btnTryAgain.isHidden = false
        loadingView.btnTryAgain.isUserInteractionEnabled = true
    }

    private func setupView(inSuperview superView: UIView) {
        removeLoading()

        superView.fixInView(subView: loadingView)
        loadingView.btnTryAgain.addTarget(self, action: #selector(didTapPressedAgain), for: .touchUpInside)
    }

    @objc
    private func didTapPressedAgain() {
        delegate?.didTapPressedAgain()
    }

    func removeLoading() {
        loadingView.removeFromSuperview()
    }
}
