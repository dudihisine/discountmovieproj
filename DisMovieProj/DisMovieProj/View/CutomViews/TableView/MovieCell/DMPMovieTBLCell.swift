//
//  DMPMovieTBLCell.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 02/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit
import AlamofireImage

class DMPMovieTBLCell: UITableViewCell {

    static let cellIdentifier = "DMPMovieTBLCell"
    static let estimatedHeight : CGFloat = 150.0
    static let cellRatio : CGFloat = estimatedHeight/320.0

    //MARK:Outlets
    @IBOutlet weak var vRoundBG: DMPRoundView!
    @IBOutlet weak var imgPoster: DMPRoundImageView!
    @IBOutlet weak var lblTitle: UILabel!

    //MARK:Lifeycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setpUI()
    }

    //MARK:Setup
    func setpUI(){
        contentView.backgroundColor = Colors.mainBG
        lblTitle.textColor = Colors.labelHighlited
        vRoundBG.backgroundColor = Colors.movieCellBG
        vRoundBG.shadowColor = Colors.shadow
    }

    func setupCell(for movie : DMPMovieViewData, arrSelectedGenres : [Genre]){
        if let posterURL = movie.posterURL{
            imgPoster.af.setImage(withURL: posterURL,  cacheKey:posterURL.absoluteString, placeholderImage: UIImage(named: "posterPlaceholder"))
        }

        let shouldHighlight = movie.shouldHighlightMovie(arrGenres: arrSelectedGenres)

        lblTitle.text = movie.title

        vRoundBG.backgroundColor = shouldHighlight ? Colors.labelHighlited : Colors.movieCellBG
        lblTitle.textColor = shouldHighlight ? Colors.label : Colors.labelHighlited

        //Accesibility
        lblTitle.accessibilityTraits = shouldHighlight ? [UIAccessibilityTraits.button,UIAccessibilityTraits.selected] : [UIAccessibilityTraits.button]
    }
    
}
