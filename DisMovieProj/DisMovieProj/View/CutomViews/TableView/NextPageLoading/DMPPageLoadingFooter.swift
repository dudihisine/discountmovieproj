//
//  DMPPageLoadingFooter.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 03/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

protocol DMPPageLoadingFooterDelegate: AnyObject {
    func didTapPressedAgain()
}

class DMPPageLoadingFooter: DMPLoadingView {

    static let footerIdentifier = "DMPPageLoadingFooter"
    static let estimatedHeight : CGFloat = 75.0

    //MARK:Outlets
    @IBOutlet weak var vError: UIView!

    //MARK:Veriables
    weak var delegate: DMPPageLoadingFooterDelegate?

    //MARK:Instance
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setupView()
    }

    //MARK:Setup
    private func setupView() {
        btnTryAgain.addTarget(self, action: #selector(didTapTryAgain), for: .touchUpInside)
    }

    //MARK:Methods
    @objc private func didTapTryAgain() {
        delegate?.didTapPressedAgain()
    }

    func showLoading() {
        vError.isHidden = true
        indicator.startAnimating()
        lblMessage.text = "Loading..."
    }

    func hideLoading() {
        vError.isHidden = true
        indicator.stopAnimating()
    }

    func showError(error : DMPError) {
        vError.isHidden = false
        lblMessage.text = error.getStringValue()
        indicator.stopAnimating()
    }

}
