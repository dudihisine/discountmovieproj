//
//  DMPVideosView.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 04/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

protocol DMPVideosViewDelegate: AnyObject{
    func playVideo(for url : URL)
}

class DMPVideosView: DMPBaseCustomView {

    //MARK:Outlets
    @IBOutlet weak var cvVideos: UICollectionView!{
        didSet{
            //MARK:Register cells
            self.cvVideos.register(UINib(nibName: DMPVideoCell.cellIdentifier, bundle: Bundle.main), forCellWithReuseIdentifier: DMPVideoCell.cellIdentifier)
        }
    }

    //MARK:Veriables
    private lazy var loadingView = DMPloadingViewManager(delegate: self)
    private lazy var viewManger = DMPVideosViewManager(delegate: self)
    private weak var delegate : DMPVideosViewDelegate?
    private var movieID : String?{
        didSet{
            if let movieID = movieID{
                viewManger.loadData(for: movieID)
            }else{
                showError(error: .general(reason: "Cant find movieID"))
            }
        }
    }

    //MARK:Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()

        self.cvVideos.delegate = self
        self.cvVideos.dataSource = self
    }

    //MARK:Methods
    func loadVideos(for movieID : String, delegate : DMPVideosViewDelegate){
        self.movieID = movieID
        self.delegate = delegate
    }

    func didLoadVideos() -> Bool {
        if let videos = viewManger.getVideos(), videos.count > 0{
            return true
        }
        return false
    }

    func playFirstVideo(){
        if let arrVideos = viewManger.getVideos(), let delegate = self.delegate, let url = arrVideos[0].url{
            delegate.playVideo(for: url)
        }
    }

}

//MARK:DMPVideosViewManagerDelegate
extension DMPVideosView: DMPVideosViewManagerDelegate{
    func startLoading() {
        self.loadingView.showLoading(superView: self.contentView)
    }

    func stopLoading() {
        self.loadingView.removeLoading()
    }

    func showError(error: DMPError) {
        self.loadingView.showError(superView: self.contentView, error: error)
    }

    func reloadList() {
        cvVideos.reloadData()
    }
}

//MARK:UICollectionViewDelegate
extension DMPVideosView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let arrVideos = viewManger.getVideos(){
            return arrVideos.count
        }

        return 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let arrVideos = viewManger.getVideos(),let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DMPVideoCell.cellIdentifier, for: indexPath) as? DMPVideoCell{
            cell.setupCell(for: arrVideos[indexPath.row], index: indexPath.row + 1)
            return cell
        }

        return UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.height * DMPVideoCell.cellRatio, height: collectionView.frame.height)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let arrVideos = viewManger.getVideos(), let delegate = self.delegate, let url = arrVideos[indexPath.row].url{
            delegate.playVideo(for: url)
        }
    }
}

//MARK:DMPLoadingViewDelegate
extension DMPVideosView: DMPLoadingViewDelegate {
    func didTapPressedAgain() {
        if let movieID = movieID{
            viewManger.loadData(for: movieID)
        }else{
            showError(error: .general(reason: "Cant find movieID"))
        }
    }
}
