//
//  DMPVideosViewManager.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 04/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

protocol DMPVideosViewManagerDelegate: AnyObject {
    func startLoading()
    func stopLoading()
    func showError(error : DMPError)
    func reloadList()
}

class DMPVideosViewManager: NSObject {

    //MARK:Veriables
    private weak var delegate : DMPVideosViewManagerDelegate?
    private var requestBeingMade = false
    private var lastRequestFailed = false
    fileprivate var arrVideos : [DMPVideoViewData]?

    //MARK:Instance
    init(delegate : DMPVideosViewManagerDelegate?) {
        self.delegate = delegate
    }

    //MARK:Data Methods
    func loadData(for movieID : String){

        showLoadingState()

        startRequest()

        DMPNetworkManager.request(request: DMPGetMovieVideosRequest(movieID: movieID), decodeType: DMPGetMovieVideosData.self) { [weak self] (result) in
            guard let self = self, let delegate = self.delegate else {return}
            switch result{
            case .success(let response):
                if let response = response, let videos = response.results{
                    let viewData = videos.map { DMPVideoViewData(video: $0) }

                    self.arrVideos = viewData

                    delegate.stopLoading()
                    delegate.reloadList()

                    self.finishRequest(withError: false)
                }else{
                    self.showErrorState(error: DMPError.general(reason: "Cant Find Data"))
                    self.finishRequest(withError: true)
                }
                break
            case .failure(let error):
                self.showErrorState(error: error)
                self.finishRequest(withError: true)
                break
            }
        }
    }

    //MARK:Methods
    func getVideos() -> [DMPVideoViewData]?{
        return arrVideos
    }

    private func showLoadingState() {
        guard let delegate = self.delegate else {return}

        delegate.startLoading()
    }

    private func showErrorState(error: DMPError) {
        guard let delegate = self.delegate else {return}

        delegate.showError(error: error)
    }

    private func startRequest() {
        requestBeingMade = true
        lastRequestFailed = false
    }

    private func finishRequest(withError error: Bool) {
        requestBeingMade = false
        lastRequestFailed = error
    }
    
}
