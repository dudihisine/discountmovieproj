//
//  DMPVideoViewData.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 04/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

extension URI{
    static let baseYoutube = "https://www.youtube.com/embed/"
    static let baseYoutubeImage = "http://img.youtube.com/vi/{videoKey}/0.jpg"
}

enum VideoType: String{
    case youTube = "YouTube"

    func getVideoURL(for videoKey : String) -> URL?{
        switch self {
        case .youTube:
            return URL(string: "\(URI.baseYoutube)\(videoKey)")
        }
    }

    func getVideoThumbnailImage(for videoKey : String) -> URL?{
        switch self {
        case .youTube:
            return URL(string:URI.baseYoutubeImage.replacingOccurrences(of: "{videoKey}", with: videoKey))
        }
    }
}

class DMPVideoViewData: NSObject {

    private let video: Video!

    init(video: Video) {
        self.video = video
    }

    var thumbnailImageURL : URL?{
        guard let key = video.key, let type = video.site, let videoType = VideoType(rawValue: type) else {return nil}

        return videoType.getVideoThumbnailImage(for: key)
    }

    var url : URL?{
        guard let key = video.key, let type = video.site, let videoType = VideoType(rawValue: type) else {return nil}

        return videoType.getVideoURL(for: key)
    }
}
