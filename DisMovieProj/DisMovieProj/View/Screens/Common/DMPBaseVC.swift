//
//  DMPBaseVC.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 03/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

class DMPBaseVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    func setupUI(){
        view.backgroundColor = Colors.mainBG

        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: Colors.labelHighlited]
        navigationController?.navigationBar.tintColor = Colors.labelHighlited
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true

    }

}
