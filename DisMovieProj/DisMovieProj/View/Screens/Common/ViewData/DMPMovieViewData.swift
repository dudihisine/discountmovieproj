//
//  DMPMovieViewData.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 02/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

class DMPMovieViewData: NSObject {
    private let movie: Movie
    private var genres : [Genre]?

    init(movie: Movie, arrGenres: [Genre]) {
        self.movie = movie
        if let genreIds = movie.genreIds{
            self.genres = arrGenres.filter { (genre) -> Bool in
                genreIds.contains(genre.id ?? -1)
            }
        }
    }

    var id: String?{
        if let movieID = movie.id{
            return String(movieID)
        }
        return nil
    }

    var title: String? {
        return movie.title
    }

    var overview: String? {
        return movie.overview
    }

    var fullGenres: String {
        if let genres = genres{
            return genres
            .map { $0.name ?? ""}
            .joined(separator: ", ")
        }

        return ""
    }

    func shouldHighlightMovie(arrGenres : [Genre]) ->Bool{
        if let genreIds = movie.genreIds{
            for genre in arrGenres{
                return genreIds.contains(genre.id ?? -1)
            }
        }

        return false
    }

    var vote: String{
        if let vote = movie.voteAverage{
            return NSString(format: "%.1f", vote) as String
        }

        return "-"
    }

    var releaseDate: String? {
        return movie.releaseDate
    }

    var posterURL: URL? {
        if let posterPath = movie.posterPath {
            return createImageURL(path: posterPath)
        }

        return nil
    }

    var backdropURL: URL? {
        if let backdropPath = movie.backdropPath {
            return createImageURL(path: backdropPath)
        }

        return nil
    }

    private func createImageURL(path: String) -> URL {
        return URL(string: "\(URI.basePoster)\(path)")!
    }

}
