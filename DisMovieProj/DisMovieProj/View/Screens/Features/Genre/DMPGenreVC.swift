//
//  DMPGenreVC.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 04/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit

protocol DMPGenreVCDelegate {
    func didSelect(genre : Genre)
}

class DMPGenreVC: DMPBaseVC {

    //MARK:Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            collectionView.register(UINib(nibName: DMPGenreCell.cellIdentifier, bundle: Bundle.main), forCellWithReuseIdentifier: DMPGenreCell.cellIdentifier)
        }
    }
    @IBOutlet weak var btnClose: UIButton!

    //MARK:Veriables
    private var delegate : DMPGenreVCDelegate!
    private var arrGenres : [Genre]!
    private var arrSelectedGenres = [Genre]()

    //MARK:Instance
    static func instance(arrGenres: [Genre],arrSelectedGenres : [Genre], delegate: DMPGenreVCDelegate) -> DMPGenreVC {
        let vc = DMPGenreVC.instantiate(fromStoryboard: "DMPGenreVC")
        vc.delegate = delegate
        vc.arrGenres = arrGenres
        vc.arrSelectedGenres = arrSelectedGenres
        return vc
    }

    //MARK:Setup
    override func setupUI() {
        super.setupUI()
        lblTitle.tintColor = Colors.labelHighlited
        btnClose.tintColor = Colors.label
        btnClose.backgroundColor = Colors.labelHighlited
        setupCollectionView()
    }

    func setupCollectionView(){
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    //MARK:Methods
    @IBAction func closeBtnPressed(_ sender: Any) {
        dismiss(animated: true)
    }
}

//MARK:UICollectionViewDelegate
extension DMPGenreVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrGenres.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DMPGenreCell.cellIdentifier, for: indexPath)

        if let genreCell = cell as? DMPGenreCell{
            genreCell.setupCell(title: arrGenres[indexPath.row].name, shouldHighlight: arrSelectedGenres.contains(where: { (genre) -> Bool in
                genre.id == arrGenres[indexPath.row].id
            }))
        }

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedGenre = arrGenres[indexPath.row]
        delegate.didSelect(genre: selectedGenre)

        if arrSelectedGenres.contains(where: { (genre) -> Bool in
            genre.id == selectedGenre.id
        }){
            arrSelectedGenres.removeAll(where: { (genre) -> Bool in
                genre.id == selectedGenre.id
            })
        }else{
            arrSelectedGenres.append(selectedGenre)
        }

        collectionView.reloadItems(at: [indexPath])
    }
}


class DMPSelfSizeCollectionView: UICollectionView {

  override var contentSize: CGSize {
    didSet {
      self.invalidateIntrinsicContentSize()
    }
  }

  override var intrinsicContentSize: CGSize {
    self.layoutIfNeeded()
    return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height + 20)
  }

}
