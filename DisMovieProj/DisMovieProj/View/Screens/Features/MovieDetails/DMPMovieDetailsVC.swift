//
//  DMPMovieDetailsVC.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 03/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit
import AlamofireImage

class DMPMovieDetailsVC: DMPBaseVC {

    static let segueIdentifier = "GoToMovieDetails"

    //MARK:Outlets
    @IBOutlet weak var ivBackdrop: UIImageView!
    @IBOutlet weak var vPosterAngleView: DMPAngleView!
    @IBOutlet weak var vRatingContainer: DMPRoundView!
    @IBOutlet weak var lblVote: UILabel!
    @IBOutlet weak var vTrailerBtnContainer: DMPRoundView!
    @IBOutlet weak var btnGoToTrailers: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var vProfile: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblGenres: UILabel!
    @IBOutlet weak var lblReleaseDateTitle: UILabel!
    @IBOutlet weak var lblReleaseDate: UILabel!
    @IBOutlet weak var lblOverviewTitle: UILabel!
    @IBOutlet weak var lblOverview: UILabel!

    @IBOutlet weak var lblVideosTitle: UILabel!
    @IBOutlet weak var vVideosView: DMPVideosView!

    //MARK:Variables
    private var movieViewData: DMPMovieViewData?

    //MARK:Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        setupMovieUI()
    }

    //MARK:Setup
    override func setupUI(){
        super.setupUI()

        vPosterAngleView.fillColor = Colors.mainBG
        vRatingContainer.backgroundColor = Colors.labelHighlited
        lblVote.textColor = Colors.label
        vRatingContainer.shadowColor = Colors.shadow
        vTrailerBtnContainer.backgroundColor = Colors.navigationBar
        vTrailerBtnContainer.shadowColor = Colors.shadow
        btnGoToTrailers.tintColor = Colors.labelHighlited
        vProfile.backgroundColor = Colors.mainBG

        lblTitle.textColor = Colors.labelHighlited
        lblGenres.textColor = Colors.labelGray
        lblReleaseDateTitle.textColor = Colors.label
        lblReleaseDate.textColor = Colors.labelGray
        lblOverviewTitle.textColor = Colors.label
        lblOverview.textColor = Colors.labelGray

        lblVideosTitle.textColor = Colors.label

        scrollView.alwaysBounceVertical = true
    }

    func load(movie : DMPMovieViewData){
        self.movieViewData = movie
    }

    func setupMovieUI(){
        if let movie = self.movieViewData{
            lblTitle.text = movie.title

            lblGenres.text = movie.fullGenres
            lblOverview.text = movie.overview
            lblReleaseDate.text = movie.releaseDate
            lblVote.text = movie.vote

            if let backdropURL = movie.backdropURL{
                ivBackdrop.af.setImage(withURL: backdropURL,  cacheKey:backdropURL.absoluteString, placeholderImage: UIImage(named: "posterPlaceholder"))
            }

            if let movieId = movie.id{
                vVideosView.loadVideos(for: movieId, delegate: self)
            }
        }

        setupAccessibility()
    }

    func setupAccessibility(){
        lblVote.accessibilityLabel = "Rating : \(lblVote.text ?? "")"
        lblTitle.accessibilityLabel = "Movie Title : \(lblTitle.text ?? "")"
        lblGenres.accessibilityLabel = "Genres : \(lblGenres.text ?? "")"
    }

    //MARK:Methods
    @IBAction func playTrailerBtnPressed(_ sender: Any) {
        if vVideosView.didLoadVideos(){
            vVideosView.playFirstVideo()
            return
        }

        //else scroll to videos

        scrollView.scrollToView(view: vVideosView, animated: true)
    }
}

//MARK:DMPVideosViewDelegate
extension DMPMovieDetailsVC: DMPVideosViewDelegate{
    func playVideo(for url: URL) {

        let vc = DMPVideoPlayerVC.instance(videoURL: url)
        if #available(iOS 13, *) {
            present(vc, animated: true)
        } else {
            show(vc, sender: nil)
        }
    }
}
