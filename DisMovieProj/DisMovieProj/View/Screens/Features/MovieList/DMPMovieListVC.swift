//
//  DMPMovieListVC.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 01/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit


class DMPMovieListVC: DMPBaseVC {

    //MARK:Outlets
    @IBOutlet weak var tblMovies: UITableView!{
        didSet{
            //MARK:Register cells
            self.tblMovies.register(UINib(nibName: DMPMovieTBLCell.cellIdentifier, bundle: Bundle.main), forCellReuseIdentifier: DMPMovieTBLCell.cellIdentifier)
            self.tblMovies.separatorStyle = .none
        }
    }

    //MARK:Variables
    private lazy var loadingView = DMPloadingViewManager(delegate: self)
    private lazy var pageLoadingFooter: DMPPageLoadingFooter = {
        let frame = CGRect(x: 0, y: 0, width: tblMovies.bounds.width, height: DMPPageLoadingFooter.estimatedHeight)
        let view = DMPPageLoadingFooter(frame: frame)
        view.delegate = self
        return view
    }()
    private lazy var screenManger = DMPMovieListVCManager(delegate: self)

    //MARK:Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        screenManger.loadData()
    }

    //MARK:Setup
    override func setupUI(){
        super.setupUI()

        title = "Movie list"
        setupTableView()

        navigationItem.rightBarButtonItems = [UIBarButtonItem(title: "Genres", style: .plain, target: self, action: #selector(openGenreScreen))]
    }

    func setupTableView(){
        tblMovies.isHidden = true
        tblMovies.delegate = self
        tblMovies.dataSource = self
        tblMovies.rowHeight = UITableView.automaticDimension
        tblMovies.estimatedRowHeight = DMPMovieTBLCell.estimatedHeight
        tblMovies.tableFooterView = pageLoadingFooter
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let movieDetailsVC = segue.destination as? DMPMovieDetailsVC, let movie = sender as? DMPMovieViewData{
            //load movie to next vc
            movieDetailsVC.load(movie: movie)
        }
    }

    //MARK:Methods
    func goToMovieDetails(movie : DMPMovieViewData){
        performSegue(withIdentifier: DMPMovieDetailsVC.segueIdentifier, sender: movie)
    }

    @objc func openGenreScreen(){
        let vc = DMPGenreVC.instance(arrGenres: screenManger.getGenres(), arrSelectedGenres: screenManger.getSelectedGenres(), delegate: self)
        if #available(iOS 13, *) {
            present(vc, animated: true)
        } else {
            show(vc, sender: nil)
        }
    }
}

//MARK:DMPMovieListVCManagerDelegate
extension DMPMovieListVC : DMPMovieListVCManagerDelegate{

    func startLoading() {
        self.loadingView.showLoading(superView: self.view)
    }

    func stopLoading() {
        self.loadingView.removeLoading()
        self.pageLoadingFooter.hideLoading()
    }

    func showError(error: DMPError) {
        self.loadingView.showError(superView: self.view, error: error)
    }

    func startNextPageLoading() {
        self.pageLoadingFooter.showLoading()
    }

    func showNextPageError(error: DMPError) {
        self.pageLoadingFooter.showError(error: error)
    }

    func reloadList() {
        tblMovies.reloadData()
        tblMovies.isHidden = false
    }

    func didLoadLastPage() {
        self.pageLoadingFooter.hideLoading()
    }

    func showGenresScreen() {
        openGenreScreen()
    }
}

//MARK:UITableViewDelegate
extension DMPMovieListVC : UITableViewDelegate, UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let arrMovies = screenManger.getMovies(){
            return arrMovies.count
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let arrMovies = screenManger.getMovies(),let cell = tableView.dequeueReusableCell(withIdentifier: DMPMovieTBLCell.cellIdentifier) as? DMPMovieTBLCell{
            cell.setupCell(for: arrMovies[indexPath.row], arrSelectedGenres: screenManger.getSelectedGenres())
            return cell
        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.width * DMPMovieTBLCell.cellRatio
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let arrMovies = screenManger.getMovies(){
            let movie = arrMovies[indexPath.row]
            goToMovieDetails(movie: movie)
        }
    }
}

//MARK:DMPLoadingViewDelegate,DMPPageLoadingFooterDelegate
extension DMPMovieListVC: DMPLoadingViewDelegate, DMPPageLoadingFooterDelegate {
    func didTapPressedAgain() {
        screenManger.loadData()
    }
}

//MARK:ScrollViewDelegate
extension DMPMovieListVC {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let lastIndexVisible = tblMovies.indexPathsForVisibleRows?.last{
            if shouldLoadNextPage(lastIndexVisible) {
                screenManger.loadData()
            }
        }
    }

    private func shouldLoadNextPage(_ lastIndexVisible: IndexPath) -> Bool {
        guard let moviesArr = screenManger.getMovies(), screenManger.shouldLoadNextPage() else { return false }

        return moviesArr.count - lastIndexVisible.row < screenManger.numberOfItemsInPage() / 2
    }
}

extension DMPMovieListVC: DMPGenreVCDelegate{
    func didSelect(genre: Genre) {
        screenManger.didSelectGenre(selectedGenre: genre)
    }
}
