//
//  DMPMovieListVCManager.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 02/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit
import Alamofire

protocol DMPMovieListVCManagerDelegate: AnyObject {
    func showGenresScreen()
    func startLoading()
    func startNextPageLoading()
    func stopLoading()
    func showError(error : DMPError)
    func showNextPageError(error : DMPError)
    func reloadList()
    func didLoadLastPage()
}

class DMPMovieListVCManager: NSObject{

    //MARK:Veriables
    private weak var delegate : DMPMovieListVCManagerDelegate?

    private var totalPages = 1
    private var currentPage = 1
    private var requestBeingMade = false
    private var lastRequestFailed = false

    fileprivate var arrMovies : [DMPMovieViewData]?
    fileprivate var arrGenres = [Genre]()
    fileprivate var arrSelectedGenres = [Genre]()

    //MARK:Instance
    init(delegate : DMPMovieListVCManagerDelegate?) {
        self.delegate = delegate
    }

    //MARK:Data Methods
    func loadData(){
        showLoadingState()
        startRequest()

        if arrGenres.count > 0{
            loadMovies()
        }else{
            DMPNetworkManager.request(request: DMPGetGenreRequest(), decodeType: DMPGetGenreData.self) { [weak self] (result) in
                guard let self = self, let delegate = self.delegate else {return}

                switch result{
                case .success(let response):
                    if let response = response, let genres = response.genres{
                        self.arrGenres = genres
                        self.loadMovies()
                        delegate.showGenresScreen()
                    }else{
                        self.showErrorState(error: DMPError.general(reason: "Cant Find Data"))
                        self.finishRequest(withError: true)
                    }
                    break
                case .failure(let error):
                    self.showErrorState(error: error)
                    self.finishRequest(withError: true)
                    break
                }
            }
        }
    }

    func loadMovies(){
        DMPNetworkManager.request(request: DMPGetMoviesRequest(page: currentPage), decodeType: DMPGetMoviesData.self) { [weak self] (result) in
            guard let self = self, let delegate = self.delegate else {return}

            switch result{
            case .success(let response):
                if let response = response, let movies = response.results{
                    let viewData = movies.map { DMPMovieViewData(movie: $0, arrGenres: self.arrGenres) }
                    if self.arrMovies != nil{
                        self.arrMovies?.append(contentsOf: viewData)
                    }else{
                        self.arrMovies = viewData
                    }

                    self.currentPage += 1

                    if let totalPages = response.totalPages{
                        self.totalPages = totalPages
                    }

                    delegate.stopLoading()
                    delegate.reloadList()

                    if self.currentPage == self.maxNumberPages(){
                        delegate.didLoadLastPage()
                    }

                    self.finishRequest(withError: false)
                }else{
                    self.showErrorState(error: DMPError.general(reason: "Cant Find Data"))
                    self.finishRequest(withError: true)
                }
                break
            case .failure(let error):
                self.showErrorState(error: error)
                self.finishRequest(withError: true)
                break
            }
        }
    }

    //MARK:Methods
    func getMovies() -> [DMPMovieViewData]?{
        return arrMovies
    }

    func getGenres() -> [Genre]{
        return arrGenres
    }

    func getSelectedGenres() -> [Genre]{
        return arrSelectedGenres
    }

    func didSelectGenre(selectedGenre : Genre){
        if arrSelectedGenres.contains(where: { (genre) -> Bool in
            genre.id == selectedGenre.id
        }){
            arrSelectedGenres.removeAll(where: { (genre) -> Bool in
                genre.id == selectedGenre.id
            })
        }else{
            arrSelectedGenres.append(selectedGenre)
        }

        delegate?.reloadList()
    }

    func shouldLoadNextPage() -> Bool {
        let isLastPage = currentPage == maxNumberPages()
        return !requestBeingMade && !lastRequestFailed && !isLastPage
    }

    private func showLoadingState() {
        guard let delegate = self.delegate else {return}

        let firstRequest = arrMovies?.isEmpty ?? true

        if firstRequest {
            delegate.startLoading()
        } else {
            delegate.startNextPageLoading()
        }
    }

    private func showErrorState(error: DMPError) {
        guard let delegate = self.delegate else {return}

        let firstRequest = arrMovies?.isEmpty ?? true

        if firstRequest {
            delegate.showError(error: error)
        } else {
            delegate.showNextPageError(error: error)
        }
    }

    private func startRequest() {
        requestBeingMade = true
        lastRequestFailed = false
    }

    private func finishRequest(withError error: Bool) {
        requestBeingMade = false
        lastRequestFailed = error
    }

    func numberOfItemsInPage() ->Int{
        return 20
    }

    func maxNumberPages() ->Int{
        return 5
    }

}

