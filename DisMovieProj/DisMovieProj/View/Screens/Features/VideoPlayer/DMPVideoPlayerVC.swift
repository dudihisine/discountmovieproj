//
//  DMPVideoPlayer.swift
//  DisMovieProj
//
//  Created by Dudi Hisine on 04/04/2020.
//  Copyright © 2020 dudiHisine. All rights reserved.
//

import UIKit
import WebKit

class DMPVideoPlayerVC: DMPBaseVC {

    //MARK:Outlets
    @IBOutlet weak var webView: WKWebView!

    //MARK:Veriables
    private var videoURL: URL!

    //MARK:Instance
    static func instance(videoURL: URL) -> DMPVideoPlayerVC {
        let vc = DMPVideoPlayerVC.instantiate(fromStoryboard: "DMPVideoPlayerVC")
        vc.videoURL = videoURL
        return vc
    }

    //MARK:Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        webView.load(URLRequest(url: videoURL))
    }

}
