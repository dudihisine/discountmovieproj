
# Discount Movie App

The Movie Database (TMDb) viewer for iOS

## Features
- Support iOS 13 Dark mode
- Support Accessibility
- Genre selection screen
- Movie list screen
- Movie Details screen - support trailer videos from youtube

## Requirements

- iOS 11.0+ 
- Xcode 11+
- Swift 5+

## Setup

### Install pods

Run `pod install` in the project root folder

## Dependencies

### [Cocoapods](https://github.com/CocoaPods/CocoaPods)

CocoaPods is a dependency manager for Swift and Objective-C Cocoa projects. Cocoapods is used to manage Alamofire, AlamofireImage.

### [Alamofire](https://github.com/Alamofire/Alamofire)

Alamofire is an HTTP networking library written in Swift.

### [AlamofireImage](https://github.com/Alamofire/AlamofireImage)

AlamofireImage is an image component library for Alamofire.
